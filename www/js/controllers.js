angular.module('starter.controllers', [])

.controller('DashCtrl', function($scope, Visitas, $state) {

    $scope.addVisita = function(){
      var fecha = new Date();
      var hora = fecha.getHours();
      var minuto = fecha.getMinutes();

    newVisita = {
      id: 10,
      idPersona: $state.params.idPersona,
      fecha: new Date().toJSON().slice(0,10),
      hora:  hora + ":" + minuto,
      proyecto:  this.visita.proyecto,
      pieza:  this.visita.pieza,
      cantidad:  this.visita.cantidad,
      avance:  this.visita.avance,
      reviso:  this.visita.reviso,
      acepta:  this.visita.acepta
    }   
    Visitas.add(newVisita);
       
    this.visita.proyecto = "";
    this.visita.pieza = "";
    this.visita.cantidad = "";
    this.visita.avance = "";
    this.visita.reviso = "";
    this.visita.acepta = "";

    $state.go('tab.chats')

  }
})

.controller('ChatsCtrl', function($scope, Chats,Visitas, $state) {

  $scope.visitas = Visitas.all();
  $scope.chats = Chats.all();
  $scope.remove = function(chat) {
    Chats.remove(chat);
  };
  $scope.addVisit = function(chat) {
    $state.go('tab.dash',{idPersona: chat.id})
  };
})

.controller('ChatDetailCtrl', function($scope, $stateParams, Chats, Visitas) {
  $scope.visitas = Visitas.all();
  $scope.chat = Chats.get($stateParams.chatId);
})

.controller('PersonalCtrl', function($scope, Chats, $state) {

  $scope.addPersonal = function(){
    newPersonal = {
      id: 5,
      name: this.nombre + ' ' + this.apellido,
      terminal: this.terminal,
      face: 'img/perry.png'
    }   
    Chats.add(newPersonal);
    this.apellido = "";
    this.nombre = "";
    this.terminal = "";
    $state.go('tab.chats')

  }
})

.controller('VisitasCtrl', function($scope, Visitas, $state) {
  
  $scope.visitas = Visitas.all();
 });
