angular.module('starter.services', [])


.factory('Visitas', function() {

  var visitas = [{
    id: 0,
    idPersona: 0,
    fecha: '02/02/2017',
    hora: '8:30',
    proyecto: 'Exito',
    pieza: 'Estante',
    cantidad: '0/3',
    avance: '1',
    reviso: 'Manuel Gracia',
    acepta: 'Jose Lira'
  },
  {
    id: 1,
    idPersona: 0,
    fecha: '02/02/2017',
    hora: '9:30',
    proyecto: 'Exito',
    pieza: 'Barra Proncipal',
    cantidad: '10/20',
    avance: '2',
    reviso: 'Manuel Gracia',
    acepta: 'Pedro Perez'
  },
  {
    id:1 ,
    idPersona: 1,
    fecha: '02/02/2017',
    hora: '8:30',
    proyecto: 'Exito',
    pieza: 'Mueble',
    cantidad: '1/2',
    avance: '1',
    reviso: 'Manuel Gracia',
    acepta: 'Luis Garcia'
  }];
  var visitas2 = angular.fromJson(window.localStorage['visitas'] || visitas)

  function persits(){
    window.localStorage['visitas'] = angular.toJson(visitas);
  }

  return {
    all: function() {
      return visitas;
    },    
    add: function(visita){
      visitas.push(visita);
      persits();
    }
  };
})


.factory('Chats', function() {
  
  var chats = [{
    id: 0,
    name: 'Sergio perez',
    terminal: 'Doblador',
    face: 'img/ben.png'
  }, {
    id: 1,
    name: 'Pedro Guzman',
    terminal: 'Ensamblador',
    face: 'img/max.png'
  }];
  var chats2 = angular.fromJson(window.localStorage['chats'] || chats)

  function persits(){
    window.localStorage['chats'] = angular.toJson(chats);
  }

  return {
    all: function() {
      return chats;
    },
    remove: function(chat) {
      chats.splice(chats.indexOf(chat), 1);
      persits();
    },
    get: function(chatId) {
      for (var i = 0; i < chats.length; i++) {
        if (chats[i].id === parseInt(chatId)) {
          return chats[i];
        }
      }
      return null;
    },
    add: function(personal){
      chats.push(personal);
      persits();
    }
  };
});
